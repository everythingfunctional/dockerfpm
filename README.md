dockerfpm
=========

Builds a docker container with fpm and gfortran installed, making CI for Fortran
code that much easier. It is based on the Ubuntu docker image.
